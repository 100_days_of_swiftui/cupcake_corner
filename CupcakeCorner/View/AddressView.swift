//
//  AddressView.swift
//  CupcakeCorner
//
//  Created by Hariharan S on 30/05/24.
//

import SwiftUI

struct AddressView: View {
    @Bindable var order: Order

    var body: some View {
        Form {
            Section {
                TextField(
                    "Name",
                    text: self.$order.name
                )
                TextField(
                    "Street Address",
                    text: self.$order.streetAddress
                )
                TextField(
                    "City",
                    text: self.$order.city
                )
                TextField(
                    "Zip",
                    text: self.$order.zip
                )
            }

            Section {
                NavigationLink("Check out") {
                    CheckoutView(order: order)
                }
            }
            .disabled(self.order.hasValidAddress == false)
        }
        .navigationTitle("Delivery details")
        .navigationBarTitleDisplayMode(.inline)
    }
}

#Preview {
    AddressView(order: Order())
}
