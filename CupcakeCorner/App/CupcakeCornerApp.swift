//
//  CupcakeCornerApp.swift
//  CupcakeCorner
//
//  Created by Hariharan S on 29/05/24.
//

import SwiftUI

@main
struct CupcakeCornerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
