//
//  Order.swift
//  CupcakeCorner
//
//  Created by Hariharan S on 30/05/24.
//

import Foundation
import Observation

@Observable
class Order: Codable {
    enum CodingKeys: String, CodingKey {
        case _type = "type"
        case _quantity = "quantity"
        case _specialRequestEnabled = "specialRequestEnabled"
        case _extraFrosting = "extraFrosting"
        case _addSprinkles = "addSprinkles"
        case _name = "name"
        case _city = "city"
        case _streetAddress = "streetAddress"
        case _zip = "zip"
    }
    
    static let types = [
        "Vanilla",
        "Strawberry",
        "Chocolate",
        "Rainbow"
    ]

    var type = 0
    var quantity = 3

    var extraFrosting = false
    var addSprinkles = false
    var specialRequestEnabled = false {
        didSet {
            if self.specialRequestEnabled == false {
                self.extraFrosting = false
                self.addSprinkles = false
            }
        }
    }
    
    var zip = ""
    var name = ""
    var city = ""
    var streetAddress = ""
    var hasValidAddress: Bool {
        if self.name.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ||
            self.streetAddress.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ||
            self.city.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ||
            self.zip.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            return false
        }
        return true
    }
    
    var cost: Double {
        var cost = Double(self.quantity) * 2
        cost += (Double(self.type) / 2)
        
        if self.extraFrosting {
            cost += Double(self.quantity)
        }
        if self.addSprinkles {
            cost += Double(self.quantity) / 2
        }
        return cost
    }
}
